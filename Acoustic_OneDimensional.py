#  Copyright (c) 2023.
#  Copyright held by Silvio Barandun ETH Zurich
#  All rights reserved.
# Cite this code as part of the paper:
# H. Ammari, S. Barandun, J. Cao, and F. Feppon, Edge modes in one-dimensional chains of subwavelength resonators

import numpy as np
from scipy import linalg as la
import matplotlib.pyplot as plt
import warnings
import matplotlib.cm as cm
from fractions import Fraction
from dataclasses import dataclass

"""
Bibliography:
[1] F Feppon, Z Cheng, H Ammari. Subwavelength resonances in 1D high-contrast acoustic media. 2022.
hal-03697696, https://hal.archives-ouvertes.fr/hal-03697696/document
"""


@dataclass
class constants:
    path_output_figures = ''
    width = 6.4
    figsz = (width,4.8/6.4*width)

class PeriodicWaveProblem:
    """
    Class for 1D Periodic high contrast media
    """

    def __init__(self, N: int, li=None, lij=None, v=1, vb=None, omega=None, delta=1):
        """
        Creates a geometrical set up with physical parameters
        :param N: number of resonators
        :param li: list or None, if list: lengths of the resonators, if None a random list will be generated
        :param lij: list or None, if list: distances between the resonators, if None a random list will be generated
        :param v: float phase velocity outside of the resonators
        :param vb: array, phase velocity inside the resonators, one entry for each resonator
        :param omega: angular frequency
        :param delta: contrast parameter
        """
        self.N = N
        if type(li) is int:
            li = [li] * N
        elif li is None:
            self.li = np.random.randint(1, 5, size=(N,))
        self.li = np.array(li)

        if type(lij) is int:
            lij = [lij] * N
        elif lij is None:
            self.lij = self.lij = np.random.randint(1, 5, size=(N, 1))
        self.lij = np.asarray(lij)
        if not (N == self.li.shape[0]):
            self.N = self.li.shape[0]
            warnings.warn(f"\nNeed to have N=len(li), got {N} and {len(li)}. Using the latter", )

        self.L = np.sum(self.li) + np.sum(self.lij)
        # l is the array with the distances between the interesting points
        l = np.zeros(2 * self.N)
        l[::2] = self.li
        l[1::2] = self.lij

        self.xi = np.insert(np.cumsum(l), 0, 0)
        self.xiCol = np.column_stack((self.xi[0:-2:2], self.xi[1::2]))
        self.xim = self.xiCol[:, 0]
        self.xip = self.xiCol[:, 1]

        if type(vb) in [list, np.ndarray]:
            assert len(
                vb) == self.N, f"Need to have vb to be a scalar or list with len(vb)=self.N. Got list with len {len(vb)} instead of {self.N}"
        if type(vb) in [float, int, complex]:
            vb = [vb] * self.N
        vb = np.array(vb)
        self.v = v
        self.vb = vb
        self.omega = omega
        self.delta = delta
        self.k = self.omega / self.v if not (omega is None) else None
        self.kb = self.omega / self.vb if not (omega is None) else None

        self.resonant_frequencies = {}
        self.resonant_modes_approx = {}
        self.resonant_modes_approx_hermitian = {}

    def get_params(self):
        params = {
            'N': self.N,
            'li': self.li,
            'lij': self.lij,
            'v': self.v,
            'vb': self.vb,
            'omega': self.omega,
            'delta': self.delta
        }
        return params

    def get_DirichletNeumann_matrix(self, alpha) -> np.ndarray:
        """
        Returns the Quasiperiodic Dirichlet to Neumann map in matrix form as given in eq 2.5 of [1]
        :param alpha: quasi periodicity
        :return: np.array
        """

        def Ak(l, k):
            return np.asarray([[-k * np.cos(k * l) / np.sin(k * l), k / np.sin(k * l)],
                               [k / np.sin(k * l), -k * np.cos(k * l) / np.sin(k * l)]])

        T = la.block_diag(- self.k * np.cos(self.k * self.lij[-1]) / np.sin(self.k * self.lij[-1]),
                          *[np.asarray(Ak(l, self.k)) for l in self.lij[:-1]],
                          - self.k * np.cos(self.k * self.lij[-1]) / np.sin(self.k * self.lij[-1]))
        T = np.array(T, dtype=complex)
        T[-1, 0] = self.k * np.exp(1j * alpha * self.L) / np.sin(self.k * self.lij[-1])
        T[0, -1] = self.k * np.exp(-1j * alpha * self.L) / np.sin(self.k * self.lij[-1])
        return T

    def getPlottingPoints(self, sampling_points=100):
        """
        Returns a sampling of points both inside and outside the resonators
        :param sampling_points: number of sample to take in intervals
        :return: np.array, np.array, points_inside, points outside
        """
        # factor = 0.002 if self.omega < 0.005 else 0.01
        pointsExt = []  # [np.linspace(-3 * np.pi / factor, -10, 500)]
        pointsExt.append(np.linspace(-10, 0, sampling_points))
        pointsExt = pointsExt + [np.linspace(self.xip[i], self.xim[i + 1], sampling_points) for i in range(self.N - 1)]
        pointsExt.append(np.linspace(self.xip[-1], self.xip[-1] + 10, sampling_points))
        # pointsExt.append(np.linspace(self.xip[-1] + 10, self.xip[-1] + 3 * np.pi / factor, 500))
        pointsExt = np.concatenate(pointsExt)

        pointsInt = [np.linspace(self.xim[i], self.xip[i], sampling_points) for i in range(self.N)]
        pointsInt = np.concatenate(pointsInt)

        return pointsInt, pointsExt

    def wf(self, f):
        """
        Solution to external problem
        :param f: values of the boundary conditions
        :return: (ai, result) where ai are the coefficients of the solution and result is a function giving the solution
        in the fundamental domain
        """
        assert len(f) == 2 * self.N, f"Need 2*N boundary condition, got {len(f)} instead of {2 * self.N}"
        if self.k == 0:
            def result(x):
                for i in range(self.N - 1):
                    if x >= self.xip[i] and x <= self.xim[i + 1]:
                        return f[2 * i] + (f[2 * i + 1] - f[2 * i]) / (self.xim[i + 1] - self.xip[i]) * (
                                x - self.xip[i])

            return [], result
        else:
            def exteriorblock(l, k, xp, xpm):
                return -1 / (2j * np.sin(k * l)) * np.asarray([[np.exp(-1j * k * xpm), -np.exp(-1j * k * xp)],
                                                               [-np.exp(1j * k * xpm), np.exp(1j * k * xp)]])

            TBlocks = la.block_diag(*(exteriorblock(l, self.k, xp, xpm)
                                      for (l, xp, xpm) in zip(self.lij, self.xip[:-1], self.xim[1:])))
            ai = TBlocks.dot(f[1:-1])

            def result(x: np.ndarray):
                y = np.zeros(x.shape, dtype=complex)
                for i in range(self.N - 1):
                    mask = (x >= self.xip[i]) * (x <= self.xim[i + 1])
                    y[mask] = ai[2 * i] * np.exp(1j * self.k * x[mask]) \
                              + ai[2 * i + 1] * np.exp(-1j * self.k * x[mask])
                return y

            return ai, result

    def getMatcalA(self, alpha: float) -> np.ndarray:
        """
        Returns A matrix from eq 3.2 of [1] to determine coefficents of solution
        :param alpha: quasi periodicity
        :return: np.ndarray
        """
        T = self.get_DirichletNeumann_matrix(alpha=alpha)
        left_blocks = [1j * self.kb[i] * np.array(
            [[-np.exp(1j * self.kb[i] * self.xim[i]), np.exp(-1j * self.kb[i] * self.xim[i])],
             [np.exp(1j * self.kb[i] * self.xip[i]), -np.exp(-1j * self.kb[i] * self.xip[i])]])
                       for i in range(self.N)]

        right_blocks = [np.array([[np.exp(1j * self.kb[i] * self.xim[i]), np.exp(-1j * self.kb[i] * self.xim[i])],
                                  [np.exp(1j * self.kb[i] * self.xip[i]), np.exp(-1j * self.kb[i] * self.xip[i])]])
                        for i in range(self.N)]

        A = la.block_diag(*left_blocks) - self.delta * T @ la.block_diag(*right_blocks)
        A = np.array(A)
        return A

    def getu(self, alpha: float):

        A = self.getMatcalA(alpha=alpha)
        assert abs(np.linalg.det(A)) > 1e-3, 'Non singular matrix, not a resonating frequence'
        coef_sol_interior = np.linalg.solve(A, np.zeros(self.N * 2))

        def u(x):
            y = np.zeros_like(x, dtype=complex)
            for i in range(self.N):
                mask = (x >= self.xim[i]) * (x <= self.xip[i])
                y[mask] = coef_sol_interior[2 * i] * np.exp(1j * self.kb[i] * x[mask]) + coef_sol_interior[
                    2 * i + 1] * np.exp(-1j * self.kb[i] * x[mask])
            return y

        f = u(self.xi[:-1])
        coef_sol_exterior, uext = self.wf(f)

        def utot(x):
            return u(x) + uext(x)

        self.coef_interior = coef_sol_interior
        self.coef_exterior = coef_sol_exterior
        self.f = f
        self.u = utot

        return utot

    def plot_u(self, alpha: float, re=True, im=False, sampling_points=100):
        """
        Plots u^\alpha
        :param alpha: quasiperiodicity
        :param re: bool, plot real part?
        :param im: bool, plot imaginary part?
        :param sampling_points: how many sampling points in each interval
        :return: None
        """
        u = self.getu(alpha=alpha)
        pointsInt, pointsExt = self.getPlottingPoints(sampling_points=sampling_points)
        fig, ax = plt.subplots(figsize=constants.figsz)
        if re:
            ax.plot(pointsInt, np.real(u(pointsInt)), '.', label='$\\Re(u)$', color='black')
            ax.plot(pointsInt, np.real(u(pointsExt)), '.', label='$\\Re(u)$', color='black')
        if im:
            ax.plot(pointsInt, np.imag(u(pointsInt)), '*', label='$\\Im(u)$', color='black')
            ax.plot(pointsExt, np.imag(u(pointsExt)), '*', label='I(u_out)', color='black')
        ax.legend()
        #ax.set_title(f'Solution of the quasiperiodic problem for $\\alpha={alpha}$')
        ax.set_xlabel('$x$')
        ax.set_ylabel('$u^\\alpha(x)$')
        #fig.show()

    def get_capacitance_matrix(self, alpha: float):
        """
        Returns quasiperiodic capacitance matrix
        :param alpha: quasiperiodicity
        :return: C^alpha
        """
        C = np.zeros((self.N, self.N), dtype=complex)
        for i in range(self.N):
            for j in range(self.N):
                if i == j - 1:
                    C[i, j] += - 1 / self.lij[i]
                if i == j:
                    C[i, j] += (1 / self.lij[j - 1] + 1 / self.lij[j])
                if i == j + 1:
                    C[i, j] += - 1 / self.lij[j]
                if (j == 0) and (i == self.N - 1):
                    C[i, j] += - np.exp(1j * alpha * self.L) / self.lij[-1]
                if (i == 0) and (j == self.N - 1):
                    C[i, j] += - np.exp(-1j * alpha * self.L) / self.lij[-1]
        self.C = C
        return C

    def get_generalized_capacitance_matrix(self, alpha: float):
        """
        Returns generalized capacitance matrix
        :param alpha: quasiperiodicity
        :return: C_G^\alpha
        """
        V = np.diag(self.li)
        S2 = np.diag(self.vb)
        S2 = S2@S2
        Cgen = S2 @ np.linalg.inv(V) @ self.get_capacitance_matrix(alpha=alpha)
        self.Cgen = Cgen
        return Cgen

    def get_resonant_frequencies(self, alpha: float):
        """
        Computes resonant frequencies using asymptotic analyisis
        :param alpha: quasiperiodicity
        :return: eigenvalues, right-and left eigenvectors of the generalised capacitance matrix
        """
        C = self.get_generalized_capacitance_matrix(alpha=alpha)
        assert self.N == 2, 'Implemented only for periodic dimers'
        a = C[0, 0]
        b = C[0, 1]
        c = C[1, 0]
        d = C[1, 1]
        sq = a ** 2 - 2 * a * d + 4 * b * c + d ** 2
        eva0 = ((a + d) - np.sqrt(sq)) / 2
        eva1 = ((a + d) + np.sqrt(sq)) / 2
        eva = np.array([eva0, eva1], dtype=complex)
        # Eigenvectors
        eve0 = np.array([-((d - a) + np.sqrt(sq)), 2 * c], dtype=complex)
        eve0 = eve0 / np.linalg.norm(eve0)
        # eve1 = np.array([-1j * np.imag(x) - np.sqrt(-np.imag(y) ** 2 + np.abs(y) ** 2), np.conj(y)])
        eve1 = np.array([-((d - a) - np.sqrt(sq)), 2 * c], dtype=complex)
        eve1 = eve1 / np.linalg.norm(eve0)

        eve = np.zeros((2, 2), dtype=complex)
        eve[:, 0] = eve0
        eve[:, 1] = eve1

        det = np.linalg.det(eve)

        eveH = np.array([[eve[1, 1], -eve[0, 1]],
                         [-eve[1, 0], eve[0, 0]]]) / det
        eveH = np.conj(eveH).T
        return eva, eve, eveH

    def get_all_resonant_frequencies(self, sample_points=1000):
        """
        Computes the resonant frequencies and approximation of eigenmodes based on generalised capacitance matrix for all
        quasiperiodicities. Returns resonant frequencies
        :param sample_points: used in sampling the interval [-pi/L,pi/L)
        :return: dict, resonant frequencies
        """
        alphas = np.linspace(-np.pi / self.L,
                             np.pi / self.L,
                             sample_points)
        eva_prev = None
        for alpha in alphas:
            eva, eve, eveH = self.get_resonant_frequencies(alpha=alpha)

            if not (eva_prev is None):
                if np.linalg.norm(eva - eva_prev) > np.linalg.norm(
                        np.flip(eva) - eva_prev):
                    eva = np.flip(eva)
                    eve = np.flip(eve, axis=1)
                    eveH = np.flip(eveH, axis=1)

            res_freq = np.zeros_like(eva,dtype=complex)
            res_freq[:eva.shape[0]] = np.sqrt(self.delta * eva)
            self.resonant_frequencies[alpha] = res_freq
            self.resonant_modes_approx[alpha] = eve
            self.resonant_modes_approx_hermitian[alpha] = eveH
            eva_prev = eva
        return self.resonant_frequencies

    def plot_resonant_frequencies(self,
                                  type,
                                  re=True,
                                  im=False,
                                  edgemode=False,
                                  bw=False,
                                  save=False):
        """
        Plots the resonant frequencies in various setings
        :param type: string in ['sep', 'single', 'complex']
        :param re: bool, plot real part?
        :param im: bool, plot imaginary part?
        :param edgemode: include edgemode frequency in the plot?
        :param save: bool, save plot?
        :return: None
        """
        assert self.resonant_frequencies, "Resonant frequencies not yet computed. Please run get_all_resonant_frequencies()"
        alphas = np.array(list(self.resonant_frequencies.keys()))
        freq = np.stack(list(self.resonant_frequencies.values()))
        if type == 'sep':
            fig, axs = plt.subplots(self.N, figsize=constants.figsz)
            for i in range(self.N):
                ax = axs[i]
                if re:
                    ax.plot(alphas, np.real(freq[:, i]), '.', label=f'w_{i}')
                if im:
                    ax.plot(alphas, np.imag(freq[:, i]), '.', label=f'w_{i}')
        elif type == 'single':
            fig, ax = plt.subplots(figsize=constants.figsz)

            for i in range(self.N):
                if re:
                    ax.plot(alphas, np.real(freq[:, i]), '.', label=f'$\\Re(\\omega_{i})$', color='black' if bw else cm.Set1.colors[i])
                if im:
                    ax.plot(alphas, np.imag(freq[:, i]), '.', label=f'$\\Im(\\omega_{i})$)', color='black' if bw else cm.Set2.colors[i])
            if edgemode:
                if re:
                    ax.plot(alphas, np.real(np.ones_like(alphas) * self.compute_edge_mode_frequency()), '.',
                            label=f'$\\Re(\\omega_e)$', color='black' if bw else cm.Set1.colors[self.N])
                if im:
                    ax.plot(alphas, np.imag(np.ones_like(alphas) * self.compute_edge_mode_frequency()), '.',
                            label=f'$\\Im(\\omega_e)$', color='black' if bw else cm.Set2.colors[self.N])

            ax.legend()
            #ax.set_title('Resonant frequencies $\\omega^\\alpha$')
            K = 2
            ax.set_xticks(ticks=[(np.pi/self.L)*(k/K) for k in range(-K,K+1)])
            labs = ['$'+ f'{"-" if np.sign(Fraction(k, K).numerator)==-1 else ""}'+ '\\frac{' + f'{abs(Fraction(k, K).numerator)}' + '}{' + f'{Fraction(k, K).denominator}' + '}' + '\\frac{\\pi}{L}$' for k in range(-K,K+1)]
            labs[K] = '0'
            ax.set_xticklabels(labels=labs)

            ax.set_xlabel('$\\alpha$')
            ax.set_ylabel('$\\omega$')
            # ax.spines['left'].set_position('zero')
            # ax.spines['bottom'].set_position('zero')
            # Eliminate upper and right axes
            # ax.spines['right'].set_color('none')
            # ax.spines['top'].set_color('none')

            #fig.show()
            if save:
                filename = f"{constants.path_output_figures}" + f"ResFreq_alphdep_{'wEig' if edgemode else ''}_l0={self.li[0]}_l01={self.lij[0]}_vb0={self.vb[0]}_vb1={self.vb[1]}".replace('.',',')
                fig.savefig(
                    filename + '.pdf',
                    bbox_inches='tight')

        elif type == 'complex':
            fig, ax = plt.subplots(figsize=constants.figsz)
            for i in range(self.N):
                ax.plot(np.real(freq[:, i]), np.imag(freq[:, i]), '.', label=f'$\\omega_{i}$', color='black' if bw else cm.Set1.colors[i])
            if edgemode:
                ax.plot(np.real(self.compute_edge_mode_frequency()), np.imag(self.compute_edge_mode_frequency()), 'x',
                        label=f'$\\omega_e$', color='black' if bw else cm.Set1.colors[-1])
            ax.legend(loc='best')
            #ax.set_title('Resonant frequencies $\\omega^\\alpha$')
            ax.set_xlabel('$\\Re(\\omega)$')
            ax.set_ylabel('$\\Im(\\omega)$')
            # Move left y-axis and bottim x-axis to centre, passing through (0,0)
            #ax.spines['left'].set_position('zero')
            #ax.spines['bottom'].set_position('zero')

            # Eliminate upper and right axes
            #ax.spines['right'].set_color('none')
            #ax.spines['top'].set_color('none')
            fig.show()
            if save:
                filename = f"{constants.path_output_figures}" + f"ResFreq_inC_{'wEig' if edgemode else None}_l0={self.li[0]}_l01={self.lij[0]}_vb0={self.vb[0]}_vb1={self.vb[1]}".replace('.',',')

                fig.savefig(
                    filename + '.pdf',
                    bbox_inches='tight')

    def compute_edge_mode_frequency(self):
        assert self.N == 2, "Works only for dimers"
        assert (self.li[0] == self.li[1]) and (self.lij[0] == self.lij[1]), "Works only for boring geometry"
        assert not (self.get_b() is None), "Unbroken PT symmetry, no edge mode"

        v1 = self.vb[0]**2
        v2 = self.vb[1]**2
        b = self.get_b()
        mu = self.delta * 4*v1*v2*(b-1)*(b*v2+v1)/(b**2*v2**2-v1**2)/self.lij[0]

        return np.sqrt(mu / self.li[0])

    def get_lam(self):
        assert self.N == 2, "Implemented only for N=2"
        l12 = self.lij[0]
        l23 = self.lij[1]
        L = 2 + l12 + l23
        C11 = -(1 / l12 + 1 / l23)
        C12 = lambda alpha: 1 / l12 + np.exp(1j * alpha * L) / l23
        lam1 = C11 + C12(np.pi / L)
        lam2 = 2 * C11
        lam = (lam2 + lam1) / (lam2 - lam1)
        return lam

    def get_b(self):
        l = self.get_lam()
        v1 = self.vb[0]
        v2 = self.vb[1]
        bp = l * (1 - v1 ** 2 / v2 ** 2) + np.sqrt(l ** 2 * (1 - v1 ** 2 / v2 ** 2) ** 2 + 4 * v1 ** 2 / v2 ** 2)
        bp = bp / 2
        bm = l * (1 - v1 ** 2 / v2 ** 2) - np.sqrt(l ** 2 * (1 - v1 ** 2 / v2 ** 2) ** 2 + 4 * v1 ** 2 / v2 ** 2)
        bm = bm / 2
        if np.abs(bm) < 1:
            return bm
        elif np.abs(bp) < 1:
            return bp
        else:
            return None


    def perturbation_factor_zakphase(self, show_integrand_plot=False):
        a = 1 / self.lij[0] + 1 / self.lij[1]

        def b(alpha):
            return - 1 / self.lij[0] - np.exp(1j * alpha * self.L) / self.lij[1]

        def f(alpha):
            return a ** 2 * (self.vb[0] ** 2 - self.vb[1] ** 2) ** 2 + 4 * self.vb[0] ** 2 * self.vb[1] ** 2 * np.abs(
                b(alpha)) ** 2

        def integrand(alpha):
            return 1 / (b(alpha) * np.sqrt(f(alpha))) * (-1j * self.L * np.exp(1j * alpha * self.L) / self.lij[1])

        M = 2000
        dalpha = 2 * np.pi / self.L / M
        alphas = np.linspace(-np.pi / self.L, np.pi / self.L, M)
        integrand_values = integrand(alphas)
        if show_integrand_plot:
            fig, ax = plt.subplots()
            ax = plt.plot(alphas, integrand_values, '.')
            fig.show()
        integral = np.trapz(integrand_values, dx=dalpha)
        complete_perturbation_factor = a * (self.vb[0] ** 2 - self.vb[1] ** 2) * integral / 2
        return complete_perturbation_factor

    def get_finite_equivalent(self, n_repetition=50, omega=1, defect=None):
        """
        Returns element of class FiniteWaveProblem simulating the infinite case
        :param n_repetition: how many repetitions of the base cell
        :param omega: frequency for the final case
        :param defect: string in [None, 'parameters', 'geometry']
        :return:
        """
        assert defect in [None, 'parameters', 'geometry'], "Non supported defect type"
        params = self.get_params()
        params['N'] = params['N'] * n_repetition
        params['li'] = list(params['li']) * n_repetition
        params['lij'] = list(params['lij']) * n_repetition
        params['lij'] = params['lij'][:-1]
        if defect == 'parameters':
            assert n_repetition % 2 == 0, f"Need even n_repretition for defect 'paramters', got {n_repetition}"
            params['vb'] = list(params['vb']) * int(n_repetition / 2) + list(np.flip(params['vb'])) * int(
                n_repetition / 2)
        elif defect == 'geometry':
            assert n_repetition % 2 == 0, f"Need even n_repretition for defect 'paramters', got {n_repetition}"
            params['lij'] = list(np.flip(params['lij'])) * int(n_repetition / 2) + list(params['lij']) * int(
                n_repetition / 2)
        else:
            params['vb'] = list(params['vb']) * n_repetition
        params['omega'] = omega
        fwp = FiniteWaveProblem(**params,
                                uin=lambda x: np.zeros_like(x),
                                duin=lambda x: np.zeros_like(x))
        return fwp


class FiniteWaveProblem:
    """
    Class for 1D Finite high contrast media
    """

    def __init__(self, N: int, li=None, lij=None, v=1, vb=None, omega=None, delta=0.01, uin=None, duin=None):
        """
        Creates a geometrical set up
        :param N: number of resonators
        :param li: list or None, if list: lengths of the resonators, if None a random list will be generated
        :param lij: list or None, if list: distances between the resonators, if None a random list will be generated
        """
        self.N = N
        if lij is None:
            self.lij = self.lij = np.random.randint(1, 5, size=(N - 1, 1))
        if type(lij) in [int, float]:
            self.lij = np.array((N-1) * [lij])
        else:
            self.lij = np.array(lij)
        if li is None:
            self.li = np.random.randint(1, 5, size=(N,))
        if type(li) in [int, float]:
            self.li = np.array(N * [li])
        else:
            if not (N == len(li)):
                self.N = len(li)
                warnings.warn(f"\nNeed to have N=len(li), got {N} and {len(li)}. Using the latter", )
            self.li = np.asarray(li)

        self.L = np.sum(self.li) + np.sum(self.lij)
        # l is the array with the distances between the interesting points
        l = np.zeros(2 * self.N - 1)
        l[::2] = self.li
        l[1::2] = self.lij

        self.xi = np.insert(np.cumsum(l), 0, 0)
        self.xiCol = np.column_stack((self.xi[0::2], self.xi[1::2]))
        self.xim = self.xiCol[:, 0]
        self.xip = self.xiCol[:, 1]

        if type(vb) in [list, np.ndarray]:
            assert len(
                vb) == self.N, f"Need to have vb to be a scalar or list with len(vb)=self.N. Got list with len {len(vb)} instead of {self.N}"
        if type(vb) in [float, int, complex]:
            vb = [vb] * self.N
        vb = np.array(vb)
        # assert not (omega is None), "Need to specify a omega"
        self.v = v
        self.vb = vb
        self.omega = omega
        self.delta = delta
        self.k = self.omega / self.v if not (omega is None) else None
        self.kb = self.omega / self.vb if not (omega is None) else None
        if uin is None:
            self.uin = lambda x: np.exp(1j * self.k * x)
        else:
            self.uin = uin
        if duin is None:
            self.duin = lambda x: 1j * self.k * np.exp(1j * self.k * x)
        else:
            self.duin = duin

        self.resonant_frequencies = {}
        self.resonant_modes_approx = {}

    def set_omega(self, omega):
        """
        Updates omega and related paramters
        :param omega:
        :return:
        """
        self.omega = omega
        self.k = self.omega / self.v if not (omega is None) else None
        self.kb = self.omega / self.vb if not (omega is None) else None

    def get_DirichletNeumann_matrix(self) -> np.ndarray:
        """
        Returns the Dirichlet to Neumann map in matrix form as given in eq 2.7 of [1]
        :param alpha: quasi periodicity
        :return: np.array
        """

        def Ak(l, k):
            return np.asarray([[-k * np.cos(k * l) / np.sin(k * l), k / np.sin(k * l)],
                               [k / np.sin(k * l), -k * np.cos(k * l) / np.sin(k * l)]])

        T = la.block_diag(1j * self.k,
                          *[np.asarray(Ak(l, self.k)) for l in self.lij],
                          1j * self.k)
        T = np.array(T, dtype=complex)
        return T

    def getPlottingPoints(self, sampling_points=100, long_range=10):
        """
        Returns a sampling of points both inside and outside the resonators
        :param sampling_points: number of sample to take in intervals
        :return: np.array, np.array, points_inside, points outside
        """
        factor = 0.002 if self.omega < 0.005 else 0.01
        pointsExt = []
        pointsExt.append(np.linspace(-long_range, 0, sampling_points * int(long_range / self.li[0])))
        pointsExt = pointsExt + [np.linspace(self.xip[i], self.xim[i + 1], sampling_points * int(self.xim[i + 1] - self.xip[i])) for i in range(self.N - 1)]
        pointsExt.append(
            np.linspace(self.xip[-1], self.xip[-1] + long_range, sampling_points * int(long_range / self.li[-1])))
        pointsExt = np.concatenate(pointsExt)

        pointsInt = [np.linspace(self.xim[i], self.xip[i], sampling_points * int(self.xip[i] - self.xim[i])) for i in range(self.N)]
        pointsInt = np.concatenate(pointsInt)

        return pointsInt, pointsExt

    def wf(self, f):
        """
        Solution to external problem
        :param f: values of the boundary conditions
        :return: (ai, result) where ai are the coefficients of the solution and result is a function giving the solution
        in the fundamental domain
        """
        if f is None:
            f = (np.random.random(2 * self.N) - 1) * 2
        assert len(f) == 2 * self.N, f"Need 2*N boundary condition, got {len(f)} instead of {2 * self.N}"
        # We treat the case k == 0 separately, as here the function is just constant
        if self.k == 0:
            def result(x):
                if x < self.xim[0]:
                    return f[0]
                elif x > self.xip[-1]:
                    return f[-1]
                for i in range(self.N - 1):
                    if x > self.xip[i] and x < self.xim[i + 1]:
                        return f[2 * i] + (f[2 * i + 1] - f[2 * i]) / (self.xim[i + 1] - self.xip[i]) * (
                                x - self.xip[i])
                return np.nan

            return [], result
        else:
            def exteriorblock(l, k, xp, xpm):
                return -1 / (2j * np.sin(k * l)) * np.asarray([[np.exp(-1j * k * xpm), -np.exp(-1j * k * xp)],
                                                               [-np.exp(1j * k * xpm), np.exp(1j * k * xp)]])

            # Builds a block diagonal matrix that is used in eq 2.3 of [1]
            TBlocks = la.block_diag(*(exteriorblock(l, self.k, xp, xpm)
                                      for (l, xp, xpm) in zip(self.lij, self.xip[:-1], self.xim[1:])
                                      )
                                    )
            # And get coefficients. We only have N-1 "outside" spaces between resonators.
            # The f[0] and f[-1] are used directly for the "outside" left and right of crystal
            ai = TBlocks.dot(f[1:-1])

            def result(x: np.ndarray):
                y = np.zeros(x.shape, dtype=complex)
                # Handle outside left and right of crystal
                mask = (x < self.xim[0])
                y[mask] = f[0] * np.exp(-1j * self.k * (x[mask] - self.xim[0]))
                mask = (x > self.xip[-1])
                y[mask] = f[-1] * np.exp(1j * self.k * (x[mask] - self.xip[-1]))
                # For in between resonators use equation 2.2 by Florian
                for i in range(self.N - 1):
                    mask = (x > self.xip[i]) * (x < self.xim[i + 1])
                    y[mask] = ai[2 * i] * np.exp(1j * self.k * x[mask]) \
                              + ai[2 * i + 1] * np.exp(-1j * self.k * x[mask])
                return y

            return ai, result

    def plot_outer_problem(self, f: np.ndarray, re=True, im=False, show=True, sampling_points=100):
        """
        Plots the solution outside the resonators
        :param f: bounday conditions
        :param re: bool, whether to plot the real part of the solution
        :param im: bool, whether to plot the imaginary part of the solution
        :param show: bool, whether to show the plot
        :param sampling_points: int, number of sampling points
        :return: (fig, ax)
        """
        ai, result = self.wf(f=f)
        _, xs = self.getPlottingPoints(sampling_points=sampling_points)
        fig, ax = plt.subplots()
        if re:
            ax.plot(xs, np.real(result(xs)), '.', label='real')
        if im:
            ax.plot(xs, np.imag(result(xs)), '.', label='imaginary')
        ax.legend()
        ax.set_title('Solution outside of the resonators')
        ax.set_xlabel('x')
        ax.set_ylabel('w(x)')
        if show:
            fig.show()
        return (fig, ax)

    def getMatcalA(self) -> [np.ndarray, np.ndarray]:
        """
        Returns A matrix from eq 3.2 of [1] to determine coefficients of solution
        :return: np.ndarray
        """
        T = self.get_DirichletNeumann_matrix()
        left_blocks = [1j * self.kb[i] * np.array(
            [[-np.exp(1j * self.kb[i] * self.xim[i]), np.exp(-1j * self.kb[i] * self.xim[i])],
             [np.exp(1j * self.kb[i] * self.xip[i]), -np.exp(-1j * self.kb[i] * self.xip[i])]])
                       for i in range(self.N)]

        right_blocks = [np.array([[np.exp(1j * self.kb[i] * self.xim[i]), np.exp(-1j * self.kb[i] * self.xim[i])],
                                  [np.exp(1j * self.kb[i] * self.xip[i]), np.exp(-1j * self.kb[i] * self.xip[i])]])
                        for i in range(self.N)]

        A = la.block_diag(*left_blocks) - self.delta * T @ la.block_diag(*right_blocks)
        A = np.array(A)

        uinxi = self.uin(self.xi)
        duinp = self.duin(self.xip)
        duinm = self.duin(self.xim)

        Tuinxi = T @ uinxi
        Tuinp = Tuinxi[1::2]
        Tuinm = Tuinxi[::2]

        RHS = np.zeros((2 * self.N, 1), dtype=complex)
        for i in range(self.N):
            RHS[2 * i] = -self.delta * Tuinm[i] - self.delta * duinm[i]
            RHS[2 * i + 1] = -self.delta * Tuinp[i] - self.delta * duinp[i]
        return A, RHS

    def getu(self):
        if np.abs(self.k) < 1e-7:
            def u(x):
                return np.zeros_like(x)

            return u
        A, RHS = self.getMatcalA()
        tol = 1e-7
        resonant = abs(np.linalg.det(A)) < tol
        if resonant:
            if np.abs(np.linalg.det(A)) == 0:
                print(f"This is a resonant frequency: abs(det(A)=0")
            else:
                print(f"This is a resonant frequency: log10(abs(det(A))={np.log10(np.abs(np.linalg.det(A)))}")
            con_factor = 1e-8
            ker = la.null_space(A, rcond=con_factor)
            while ker.shape[1] == 0:
                con_factor = con_factor * 10
                ker = la.null_space(A, rcond=con_factor)
            exterior_sum = np.abs(ker[0, :]) + np.abs(ker[1, :]) + np.abs(ker[-1, :]) + np.abs(ker[-2, :])
            sorting_index = np.argsort(exterior_sum)
            ker = ker[:, sorting_index]
            coef_sol_interior = ker[:, 0]
        else:
            print(f"This is NOT a resonant frequency: log10(abs(det(a))={np.log10(np.abs(np.linalg.det(A)))}")
            coef_sol_interior = np.linalg.solve(A, RHS)

        def u_int(x):
            y = np.zeros_like(x, dtype=complex)
            for i in range(self.N):
                mask = (x >= self.xim[i]) * (x <= self.xip[i])
                y[mask] = coef_sol_interior[2 * i] * np.exp(1j * self.kb[i] * x[mask]) + coef_sol_interior[
                    2 * i + 1] * np.exp(-1j * self.kb[i] * x[mask])
            return y

        f = u_int(self.xi)
        _, uext = self.wf(f)

        def utot(x):
            return u_int(x) + uext(x)

        self.u = utot

        return utot

    def plot_u(self, re=True, im=False, sampling_points=50, long_range=10, symlog=False,
               save=False, name_addition=''):
        u = self.getu()
        pointsInt, pointsExt = self.getPlottingPoints(sampling_points=sampling_points, long_range=long_range)
        fig, ax = plt.subplots(figsize=constants.figsz)
        if re:
            ax.plot(pointsInt, np.real(u(pointsInt)), label='$\\Re(u)$', color='blue')
            ax.plot(pointsExt, np.real(u(pointsExt)), color='blue')
        if im:
            ax.plot(pointsInt, np.imag(u(pointsInt)), label='$\\Im(u)$', color='red')
            ax.plot(pointsExt, np.imag(u(pointsExt)), color='red')
        ax.legend()
        #ax.set_title(f'Solution of the finite problem')
        ax.set_xlabel('$x$')
        ax.set_ylabel('$u(x)$')
        ax.set_yscale('symlog') if symlog else None
        if save:
            filename = f'{constants.path_output_figures}plotu_' + name_addition + '.pdf'
            fig.savefig(filename, bbox_inches='tight')
        #fig.show()

    def getApproxCapacitanceMatrix(self):
        d1 = np.concatenate(([1 / self.lij[0]],
                             1 / self.lij[:-1] + 1 / self.lij[1:], [1 / self.lij[-1]]))
        d2 = - 1 / self.lij
        C = np.diag(d1) + np.diag(d2, 1) + np.diag(d2, -1)
        return C

    def getResonatFrequencies(self, verbose):
        if np.max(np.abs(
                self.vb - np.ones_like(self.vb) * self.vb[0])) < 1e-3:
            print("Computing resonant frequencies for case with constant material paramer") if verbose else None
            vb = self.vb[0]
            C = self.getApproxCapacitanceMatrix()
            B = np.diag([1] + [0] * (self.N - 2) + [1])
            resonant_freq = np.zeros(self.N * 2, dtype=complex)
            resonant_freq[:2] = np.array([0, -2j * self.delta * vb ** 2 / (self.v * sum(self.li))])
            V = np.diag(self.li)
            Vhalf = np.sqrt(np.linalg.pinv(V))
            eva, eve = np.linalg.eigh(Vhalf.dot(C).dot(Vhalf))
            sort_ind = np.argsort(eva)
            eva = eva[sort_ind]
            eve = eve[:, sort_ind]
            eve = Vhalf.dot(eve)
            mask = (eva < 0) * (np.abs(eva) < 1e-10)
            eva[mask] = 0
            aiBai = np.diag(eve[:, 1:].T.dot(B.dot(eve[:, 1:])))
            for i in range(self.N - 1):
                ind1 = 2 + i
                ind2 = 2 + self.N - 1 + i
                part1 = vb * np.sqrt(eva[i]) * np.sqrt(self.delta)
                part2 = -1j * self.delta * vb ** 2 / (2 * self.v) * aiBai[i]
                resonant_freq[ind1] = part1 + part2
                resonant_freq[ind2] = -part1 + part2
            return resonant_freq
        else:
            print("Computing resonant frequencies for case with changing material parameter") if verbose else None
            V2 = np.diag(self.vb)
            V2 = V2 @ V2
            L = np.diag(self.li)
            C_G = V2 @ np.linalg.inv(L) @ self.getApproxCapacitanceMatrix()
            eva, eve = np.linalg.eig(C_G)
            res_freq = np.sqrt(self.delta * eva)
            return res_freq

    def plot_resonant_frequencies(self, mode):
        res_freq = self.getResonatFrequencies(verbose=False)
        fig, ax = plt.subplots()
        if mode == 'complex':
            ax.plot(np.real(res_freq), np.imag(res_freq), '.')
        if mode == 'single':
            ax.plot(self.N * [self.N], np.real(res_freq), '.', label='real')
            ax.plot(self.N * [-self.N], np.imag(res_freq), '.', label='imag')
            ax.legend()
        #fig.show()

    def find_edgemodefrequency_and_idx(self):
        res_freq = self.getResonatFrequencies(verbose=False)
        res_freq = np.sort(res_freq)
        dist = np.diff(res_freq)
        ball_radius = np.zeros_like(res_freq)
        ball_radius[0] = dist[0]
        ball_radius[2 * self.N - 1] = dist[2 * self.N - 2]
        for i in range(1, 2 * self.N - 1):
            ball_radius[i] = min(dist[i - 1], dist[i])
        idxs = np.argsort(ball_radius)
        return res_freq[idxs[-2:]], idxs[-2:]

def plot_RF_increasing_length(Nmin,Nmax,step,lij,li,vb,save=False):
    lij_unit = lij
    Ns = range(Nmin, Nmax+1, step)
    freqs = {}
    for N in Ns:
        lij = list(np.flip(lij_unit)) * int(N / 4 + 1) + list(lij_unit) * int(
            N / 4 + 1)
        lij = lij[1:-1]
        fwp = FiniteWaveProblem(N=N,li=li,lij=lij,v=1,vb=vb,delta=0.001)
        res_freq = fwp.getResonatFrequencies(verbose=False)
        freqs[N] = res_freq

    pwp = PeriodicWaveProblem(N=2,li=li,lij=lij_unit,v=1,vb=vb,delta=0.001)
    pwp.get_all_resonant_frequencies()
    alphas = np.array(list(pwp.resonant_frequencies.keys()))
    freq = np.stack(list(pwp.resonant_frequencies.values()))

    fig, ax = plt.subplots(figsize=constants.figsz)
    axx = ax.twiny()
    axx.plot(alphas, freq, color='grey')
    axx.plot(alphas, -freq, color='grey')
    axx.set_xticks([])
    for key,val in freqs.items():
        ax.plot(np.ones_like(val) * key, val, '.', c='black')
    ax.set_zorder(1)  # default zorder is 0 for ax1 and ax2
    ax.patch.set_visible(False)  # prevents ax1 from hiding ax2
    ax.set_xlabel('Number of resonators')
    ax.set_ylabel('$\\omega$')
    #ax.set_title('Resonant frequencies for Hermitian casem, geometrical defect')
    fig.savefig(f'{constants.path_output_figures}convergence_resonant_frequency_HermitianEdgemode.pdf', bbox_inches='tight') if save else None
    #fig.show()

def plot_RF_with_geometrical_perturbation(N, sigma_min, sigma_max, lij, li, vb, save=False):
    lij_unit = lij
    sigmas = np.linspace(sigma_min, sigma_max, 50)
    freqs = {}
    for sigma in sigmas:
        lij = list(np.flip(lij_unit)) * int(N / 4 + 1) + list(lij_unit) * int(
            N / 4 + 1)
        lij = lij[1:-1]
        lij = np.array(lij)
        lij = lij + np.random.normal(loc=0, scale=sigma, size=lij.shape)
        fwp = FiniteWaveProblem(N=N, li=li, lij=lij, v=1, vb=vb, delta=0.001)
        res_freq = fwp.getResonatFrequencies(verbose=False)
        freqs[sigma] = res_freq
    fig, ax = plt.subplots(figsize=constants.figsz)
    for key, val in freqs.items():
        ax.plot(np.ones_like(val) * key, val, '.', c='black')
    ax.set_xlabel('$\\sigma$ of random noise')
    ax.set_ylabel('$\\omega$')
    #ax.set_title('Stability of edgemodes')
    fig.savefig(f'{constants.path_output_figures}stability_HermitianEdgemode.pdf',
                bbox_inches='tight') if save else None
    #fig.show()

def plot_RF_with_parameter_perturbation(N, sigma_min, sigma_max, lij, li, vb, save=False):
    sigmas = np.linspace(sigma_min, sigma_max, 50)
    freqs = {}
    for sigma in sigmas:
        sigma = sigma / np.sqrt(2) / np.mean(np.abs(vb))
        vbs = list(vb) * int(N / 4) + list(np.flip(vb)) * int(
            N / 4)
        vbs = np.array(vbs)
        pert = np.random.multivariate_normal(np.zeros(2), np.eye(2)*sigma, N)
        vbs = vbs + pert[:,0] + pert[:,1]*1j
        fwp = FiniteWaveProblem(N=N, li=li, lij=lij, v=1, vb=vbs, delta=0.001)
        res_freq = fwp.getResonatFrequencies(verbose=False)
        freqs[sigma] = res_freq
    fig, axs = plt.subplots(2,figsize=constants.figsz)
    for key, val in freqs.items():
        axs[0].plot(np.ones_like(val) * key, np.real(val), '.', c='black')
        axs[1].plot(np.ones_like(val) * key, np.imag(val), '.', c='black')
    axs[1].set_xlabel('$\\sigma$')
    axs[0].set_ylabel('$\\Re(\\omega)$')
    axs[1].set_ylabel('$\\Im(\\omega)$')
    #fig.suptitle('Stability of edgemodes')
    fig.savefig(f'{constants.path_output_figures}stability_nonHermitianEdgemode.pdf',
                bbox_inches='tight') if save else None
    #fig.show()

def find_isolated(res_freq):
    N = len(res_freq)
    res_freq = np.array(res_freq)
    res_freq = np.sort(res_freq)
    dist = np.diff(res_freq)
    ball_radius = np.zeros_like(res_freq)
    ball_radius[0] = dist[0]
    ball_radius[N - 1] = dist[N - 2]
    for i in range(1, N - 1):
        ball_radius[i] = min(dist[i - 1], dist[i])
    idxs = np.argsort(ball_radius)
    isolated = np.sort(res_freq[idxs[-2:]])
    return isolated