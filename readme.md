# Edge modes in one-dimensional chains of subwavelength resonators

By: H. Ammari, S. Barandun, J. Cao, and F. Feppon

Paper available at: https://arxiv.org/abs/2301.06747

Corresponing author: silvio dot barandun at sam dot math dot ethz dot ch

## Introduction
This repository contains the code related to the above mentioned paper.
